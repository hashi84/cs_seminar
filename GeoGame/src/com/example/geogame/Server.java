package com.example.geogame;


import java.util.*;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;


import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.net.Uri;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

/*
 * シングルトンな擬似サーバ
<<<<<<< HEAD
 * 位置情報から適当にゴール群を生成する
 * また、ランキング情報を管理する
=======
 * 位置情報から海を避け
 * 適当にゴール群を生成する
>>>>>>> maru
 */
public class Server {
	private static Server server;
	private List<LatLng> goalList;
	private List<RankData> ranking;
	public static final int GOAL_NUMBER = 5;
	private static final double GOAL_RANGE = 1.0;
	private MainActivity ma;
	private Position currentPos;
	
	private Server() {
		goalList = new ArrayList<LatLng>();
		ranking = generateDummyRanking();
	}
	
	public List<LatLng> getGoallist(){
		return goalList;
	}
	
	public MainActivity getMa(){
		return server.ma;
	}
	
	public static Server getInstance() {
		if(server == null)
			server = new Server();
		return server;
	}
	
	public void decideGoal(MainActivity ma,double latitude, double longitude) {
		//ゴール決定を別スレッドに投げる
		this.ma = ma;
		Uri.Builder builder = new Uri.Builder();
		currentPos = new Position(latitude, longitude);
		AsyncHttpServerRequest task = new AsyncHttpServerRequest(generateRandomPosition(latitude), generateRandomPosition(longitude));
		task.execute(builder);
	}
	
	public void onGoalDecided() {
		if(goalList.size() < GOAL_NUMBER) {
			double lat = generateRandomPosition(currentPos.getLatitude());
			double lng = generateRandomPosition(currentPos.getLongitude());
			AsyncHttpServerRequest task = new AsyncHttpServerRequest(lat, lng);
			task.execute(new Uri.Builder());
		} else {
			ma.Ongoaldecided();
		}
	}
	
	public List<RankData> getRankingData() {
		Collections.sort(ranking);
		return ranking;
	}
	
	private double generateRandomPosition(double pos) {
		return pos + Math.random() * GOAL_RANGE * ((Math.random() > 0.5) ? 1 : -1);
	}
	
	public void register(RankData data) {
		ranking.add(data);
	}
	
	/*
	 * テスト用のダミーランキングを生成
	 */
	private List<RankData> generateDummyRanking() {
		List<RankData> dummy = new ArrayList<RankData>();
		dummy.add(new RankData("player1", 5000, Calendar.getInstance()));
		dummy.add(new RankData("player2", 4000, Calendar.getInstance()));
		dummy.add(new RankData("player3", 4300, Calendar.getInstance()));
		dummy.add(new RankData("player4", 3000, Calendar.getInstance()));
		return dummy;
	}
}

class AsyncHttpServerRequest extends AsyncTask<Uri.Builder, Void, String> {
    
    private double latitude, longitude;
    Server server = Server.getInstance();

    public AsyncHttpServerRequest(double lat, double lng) {
        // 呼び出し元のアクティビティ
        latitude = lat;
        longitude = lng;
    }
    
    @Override
    protected String doInBackground(Uri.Builder... builder) {
    	HttpClient httpClient = new DefaultHttpClient();
		String uri = "http://maps.googleapis.com/maps/api/geocode/json"
				+ "?latlng=" + latitude + "," + longitude + "&sensor=true&language=ja";
		HttpGet request = new HttpGet(uri.toString());
		HttpResponse httpResponse = null;

		try {
			httpResponse = httpClient.execute(request);
		} catch(Exception e) {
			//Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
		}

		int status = httpResponse.getStatusLine().getStatusCode();

		if(HttpStatus.SC_OK == status) {
			try {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				httpResponse.getEntity().writeTo(outputStream);
				return outputStream.toString();
			} catch(Exception e) {
				//Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
			}
		} else {
			//Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
		}
    	//return outputStream.toString();
		return null;
    }


    @Override
    protected void onPostExecute(String result) {
    	JSONArray eventArray = null;
    	
    	try{
    		JSONObject rootObject = new JSONObject(result);
    		eventArray = rootObject.getJSONArray("results");
    	}
    	catch(Exception e){
    		//Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
    	}
    
    	if(!isOnTheSea(eventArray) && server.getGoallist().size() < Server.GOAL_NUMBER) {
    		server.getGoallist().add(new LatLng(latitude, longitude));
    	}
    	server.onGoalDecided();
    }
    
	private boolean isOnTheSea(JSONArray eventArray) {
		return eventArray.length() == 0;
	}
}

