package com.example.geogame;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class GameClearActivity extends Activity {
	private RankData data;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_clear_activity);
		
	}
			
	public void register(View v) {
		Intent intent = getIntent();
		int score = intent.getIntExtra(Util.KEY_SCORE, -1);
		data = new RankData("new player", score, Calendar.getInstance());
		Server server = Server.getInstance();
		server.register(data);
	}
	
	private void showRanking() {
		Intent intent = new Intent(this, RankingActivity.class);
		startActivity(intent);
	}
	
}
