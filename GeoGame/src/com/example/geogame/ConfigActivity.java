package com.example.geogame;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.gms.maps.model.LatLng;

/*
 * 目的地リストを表示する画面
 */
public class ConfigActivity extends ListActivity implements OnItemClickListener {
	List<LatLng> goalList;	//ゴール一覧
	Intent intent;
	Position destination;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Server server = Server.getInstance();
		goalList = server.getGoallist();
		
		List<String> title = new ArrayList<String>();
		for(int i = 0; i < goalList.size(); i++) {
			title.add("目的地" + i + "\n(" + goalList.get(i).latitude
					+ ", " + goalList.get(i).longitude + ")");
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.settinglist, title);
		setListAdapter(adapter);
		ListView varListView = getListView();
		varListView.setOnItemClickListener(this);
	}

	/*
	 * リストのアイテムをタッチすると呼ばれる
	 * 目的地の緯度と経度をIntentに乗せて次の画面へ
	 */
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		//Toast.makeText(this, "" + position + "番目がクリックされました", Toast.LENGTH_LONG).show();
		intent = new Intent(this, ReadyActivity.class);
		destination = new Position(goalList.get(position).latitude, goalList.get(position).longitude);
		intent.putExtra(Util.KEY_POSITION, getIntent().getSerializableExtra(Util.KEY_POSITION));
		intent.putExtra(Util.KEY_DESTINATION, destination); 
		startActivity(intent);
	}
	
	/*
	 * APIを叩いて返ってきたきたJSONを展開し、住所を取得する
	 * また、海上かどうかを判断する
	 */
	void parseJSON(String json) {
		String address = "";
		String errMsg = null;
		try {
			String res = "";
			JSONObject rootObject = new JSONObject(json);
			JSONArray eventArray = rootObject.getJSONArray("results");
			
			for(int i = 0; i < eventArray.length(); i++) {
				JSONObject jsonObject = eventArray.getJSONObject(i);
				res = jsonObject.getString("formatted_address");
				if(!res.equals(""))	{
					address = res;
					break;
				}
			}
		} catch(Exception e) {
			errMsg = e.toString();
		}
		
		//Toast.makeText(this, address, Toast.LENGTH_LONG).show();
		
		//return (errMsg == null) ? address : errMsg;
		
		//intent.putExtra(Util.KEY_ADDRESS, address);
		destination.setAddress(address);
		intent.putExtra(Util.KEY_DESTINATION, destination);
		startActivity(intent);
	}
}
