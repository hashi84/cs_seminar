package com.example.geogame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.*;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
 * ゲームのスタート画面
 * 位置情報を取得する
 */
public class MainActivity extends Activity implements LocationListener {
	LocationManager lm;
	boolean measuredPosition = false;
	private double latitude, longitude;
	boolean goaldecided = false;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();
		setContentView(R.layout.activity_main);
		lm = (LocationManager)getSystemService(LOCATION_SERVICE);
		dialog = new ProgressDialog(this);
		dialog.setMessage("Now loading...");
		dialog.show();
		Typeface tf = Typeface.createFromAsset(getAssets(), "gnuolane rg.ttf");
		((Button)findViewById(R.id.ranking)).setTypeface(tf);
		((Button)findViewById(R.id.start_button)).setTypeface(tf);
		((TextView)findViewById(R.id.title)).setTypeface(tf);
	}

	/*
	 * スタートボタンを押された際、位置情報を取得できていれば次の画面へ
	 */
	public void onClick(View v) {
		if(measuredPosition && goaldecided) {
			Intent i = new Intent(this, ConfigActivity.class);
			i.putExtra(Util.KEY_POSITION, new Position(latitude, longitude));
			startActivity(i);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);//new LocationListener() {
	}
	@Override
	public void onLocationChanged(Location lc) {
		Server server = Server.getInstance();
		latitude = lc.getLatitude();
		longitude = lc.getLongitude();
		server.decideGoal(this, latitude, longitude);
		lm.removeUpdates(this);
		measuredPosition = true;
		//((Button)findViewById(R.id.start_button)).setText("Game start");
	}
	@Override
	public void onProviderDisabled(String arg0) {}
	@Override
	public void onProviderEnabled(String arg0) {}
	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}

	public void showRanking(View v) {
		Intent intent = new Intent(this, RankingActivity.class);
		startActivity(intent);
	}
	
	public void Ongoaldecided() {
		goaldecided = true;
		dialog.dismiss();
	}
}