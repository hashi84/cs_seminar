package com.example.geogame;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class ResultActivity extends Activity {
	private int score;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result);
		getActionBar().setTitle("Result");
		Intent intent = getIntent();
		double dist = intent.getDoubleExtra(Util.KEY_DISTANCE, -1);
		score = calcScore(dist);
		int time = intent.getIntExtra(Util.KEY_TIME, -1);
		TextView scoreView, timeView, distView;
		scoreView = (TextView)findViewById(R.id.score);
		scoreView.setText("スコア: " + score);
		timeView = (TextView)findViewById(R.id.time);
		timeView.setText("経過時間: " + String.format("%02d:%02d", time / 60, time % 60));
		distView = (TextView)findViewById(R.id.dist);
		distView.setText("距離: " + dist + "km");
	}
	
	/*
	 * 移動距離からスコアを計算
	 */
	private int calcScore(double dist) {
		return (int)(dist * 1000);
	}
	
	/*
	 * サーバへスコアを送信する
	 */
	public void registerRanking(View v) {
		Server server = Server.getInstance();
		server.register(new RankData("new player", score, Calendar.getInstance()));
		Intent intent = new Intent(this, RankingActivity.class);
		startActivity(intent);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return super.onKeyDown(keyCode, event);
		}
		return false;
	}
}
