package com.example.geogame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;

/*
 * ミッション開始前の準備画面
 */
public class ReadyActivity extends FragmentActivity implements OnMapReadyCallback {
	private Position currentPos, destination;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ready);
		
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		
		Intent intent = getIntent();
		currentPos = (Position)intent.getSerializableExtra(Util.KEY_POSITION);
		destination = (Position)intent.getSerializableExtra(Util.KEY_DESTINATION);
		
		((TextView)findViewById(R.id.distance)).setText("距離: "
				+ currentPos.distanceFrom(destination) + "km");
		((TextView)findViewById(R.id.direction)).setText("方角: "
				+ currentPos.directionOf(destination));
	}

	@Override
	public void onMapReady(GoogleMap map) {
		map.addMarker(new MarkerOptions().position(destination.toLatLng())
				.title(destination.getAddress()));
		Intent intent = getIntent();
		destination = (Position)intent.getSerializableExtra(Util.KEY_DESTINATION);
		
		map.addMarker(new MarkerOptions().position(destination.toLatLng()).title(destination.getAddress()));
		
		CameraPosition cameraPos = new CameraPosition.Builder().target(destination.toLatLng())
				.zoom(8.5f).bearing(0).build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos));
	}
	
	/*
	 * スタートボタンを押されると、ゲーム開始
	 */
	public void startGame(View v) {
		Intent intent = new Intent(this, GameActivity.class);
		intent.putExtra(Util.KEY_DESTINATION, destination);
		intent.putExtra(Util.KEY_POSITION, currentPos);
		startActivity(intent);
	}
}
