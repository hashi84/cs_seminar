package com.example.geogame;

import java.util.Calendar;

public class RankData implements Comparable<RankData>{
	private String name;
	private int score;
	private Calendar cal;
	
	RankData(String name, int score, Calendar cal) {
		this.name = name;
		this.score = score;
		this.cal = cal;
	}
	
	String getName() {
		return name;
	}
	int getScore() {
		return score;
	}
	Calendar getCalendar() {
		return cal;
	}
	@Override
	public String toString() {
		return name + "\t" + score + "�_\t\n\t"
				+ cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/"
				+ cal.get(Calendar.DATE);
	}

	@Override
	public int compareTo(RankData another) {
		return another.score - score;
	}
}
