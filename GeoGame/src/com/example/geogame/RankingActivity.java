package com.example.geogame;

import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class RankingActivity extends ListActivity {
	private List<RankData> ranking;
	private Server server;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		server = Server.getInstance();
		ranking = server.getRankingData();/*
		List<String> strData = new ArrayList<String>();
		for(RankData data : ranking) {
			strData.add(data.toString());
		}*/
		
		String[] strData = new String[ranking.size()];
		for(int i = 0; i < ranking.size(); i++) {
			strData[i] = (i+1) + "��\t" + ranking.get(i).toString();
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.settinglist, strData);
		setListAdapter(adapter);
		//ListView varListView = getListView();
		//varListView.setOnItemClickListener(this);
	}
}
