package com.example.geogame;

import java.io.Serializable;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class Position implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final double GOAL_RANGE = 10.0;		//�S�[������͈̔�(km)

	private double latitude, longitude;
	private String address;
	
	public Position(double lat, double lng) {
		this.latitude = lat;
		this.longitude = lng;
	}
	
	public void movedTo(double lat, double lng) {
		this.latitude = lat;
		this.longitude = lng;
	}
	
	public void movedTo(Location location) {
		this.latitude = location.getLatitude();
		this.longitude = location.getLongitude();
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress() {
		return address;
	}
	
	public double distanceFrom(Position pos) {
		double dist = Util.calcDistance(latitude, longitude, pos.latitude, pos.longitude);
		dist = Math.round(dist);
		return dist / 1000.0;
	}
	
	public boolean arrived(Position dest) {
		return distanceFrom(dest) < GOAL_RANGE;
	}
	
	@Override
	public String toString() {
		return "(" + latitude + ", " + longitude + ")";
	}
	
	public LatLng toLatLng() {
		return new LatLng(latitude, longitude);
	}
	
	public String directionOf(Position dest) {
		double diffOfLat = latitude - dest.latitude;
		double diffOfLng = longitude - dest.longitude;
		if(Math.abs(diffOfLat) > Math.abs(diffOfLng) * 2) {
			return (diffOfLat > 0) ? "��" : "�k";
		} else if(Math.abs(diffOfLng) > Math.abs(diffOfLat) * 2) {
			return (diffOfLng > 0) ? "��" : "��";
		} else {
			return ((diffOfLat > 0) ? "��" : "�k") + ((diffOfLng > 0) ? "��" : "��");
		}
	}
}
