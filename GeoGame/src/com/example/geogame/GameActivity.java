package com.example.geogame;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

/*
 * ゲーム中の画面
 */
public class GameActivity extends Activity implements OnMapReadyCallback, LocationListener {
	private LocationManager locationManager;
	private Position currentPos, destination;
	private GoogleMap map;
	private double totalMovedDist = -1;
	private Marker marker, goalMarker;
	private TextView distanceLabel, directionLabel, timeLabel;
	private int eapsedTime;
	private Handler handler;
	private Timer timer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_screen);
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);

		Intent intent = getIntent();
		currentPos = (Position)intent.getSerializableExtra(Util.KEY_POSITION);
		destination = (Position)intent.getSerializableExtra(Util.KEY_DESTINATION);

		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		distanceLabel = (TextView)findViewById(R.id.distance);
		directionLabel = (TextView)findViewById(R.id.direction);
		timeLabel = (TextView)findViewById(R.id.eapsed_time);

		handler = new Handler();
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						eapsedTime++;
						int min, sec;
						min = eapsedTime / 60;
						sec = eapsedTime % 60;
						timeLabel.setText(String.format("経過時間: %02d:%02d", min, sec));
					}
				});
			}
		}, 1000, 1000);
	}

	@Override
	protected void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
	}
	//new LocationListener() {
	/*
	 * 位置情報が変更されると呼ばれる
	 * ゴールに十分近ければゲーム終了メソッドを呼ぶ
	 * マップが表示されていれば初回はマーカーを設置
	 * 2回目以降はマーカーを移動
	 */
	@Override
	public void onLocationChanged(Location location) {
		currentPos.movedTo(location);
		distanceLabel.setText("距離: " + currentPos.distanceFrom(destination) + "km");
		directionLabel.setText("方角: " + currentPos.directionOf(destination));

		if(totalMovedDist == -1) {	//初回のみ総移動距離を計算(これだと要移動距離,しかも直線距離だけど...)
			totalMovedDist = currentPos.distanceFrom(destination);
		}

		if(currentPos.arrived(destination)) {
			finishGame();
		}

		if(map != null) {
			//map.clear();
			BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
			goalMarker.setPosition(destination.toLatLng());
			
			if(marker == null) {
				marker = map.addMarker(new MarkerOptions().position(currentPos.toLatLng())
						.title("現在地").icon(icon));
				PolylineOptions geodesics = new PolylineOptions()
				.add(new LatLng(location.getLatitude(), location.getLongitude()), destination.toLatLng())// 2地点設定
				.geodesic(true)// 測地線
				.color(Color.RED)
				.width(3);
				map.addPolyline(geodesics);
				geodesics.visible(false);
			} else {
				marker.setPosition(currentPos.toLatLng());
			}
		}
	}

	@Override
	public void onProviderDisabled(String provider) {}
	@Override
	public void onProviderEnabled(String provider) {}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}

	/*
	 * ゲームを終了する
	 */
	private void finishGame() {
		Toast.makeText(this, "目的地に到達しました", Toast.LENGTH_LONG).show();
		timer.cancel();
		Intent i = new Intent(this, ResultActivity.class);
		i.putExtra(Util.KEY_DISTANCE, totalMovedDist);
		i.putExtra(Util.KEY_TIME, eapsedTime);
		startActivity(i);
	}

	@Override
	public void onMapReady(GoogleMap map) {
		this.map = map;
		goalMarker = map.addMarker(new MarkerOptions().position(destination.toLatLng())
				.title(destination.getAddress()));
		CameraPosition cameraPos = new CameraPosition.Builder().target(destination.toLatLng())
				.zoom(8.5f).bearing(0).build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos));
		Log.v("Map", "Zoom Level = " + map.getCameraPosition().zoom);
		map.addMarker(new MarkerOptions().position(destination.toLatLng()).title(destination.getAddress()));
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		locationManager.removeUpdates(this);
	}
}
