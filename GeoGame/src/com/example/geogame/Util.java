package com.example.geogame;

public class Util {
	public static final String KEY_DISTANCE = "distance";
	public static final String KEY_SCORE = "score";
	public static final String KEY_TIME = "time";
	public static final String KEY_POSITION = "position";
	public static final String KEY_DESTINATION = "destination";
	
	private static final double BESSEL_A = 6377397.155;
	private static final double BESSEL_E2 = 0.00667436061028297;
	private static final double BESSEL_MNUM = 6334832.10663254;
	
	/*
	 * 2地点間の距離をm単位で返す
	 */
	public static double calcDistance(double lat1, double lng1, double lat2, double lng2) {
		double my = deg2rad((lat1 + lat2) / 2.0);
		double dx = deg2rad(lat1 - lat2);
		double dy = deg2rad(lng1 - lng2);

		double sin = Math.sin(my);
		double w = Math.sqrt(1.0 - BESSEL_E2 * sin * sin);
		double m = BESSEL_MNUM / (w * w * w);
		double n = BESSEL_A / w;
		double dym = dy * m;
		double dxcos = dx * n * Math.cos(my);
		return Math.sqrt(dym * dym + dxcos * dxcos);		
	}

	private static double deg2rad(double deg) {
		return deg * Math.PI / 180.0;
	}
	
	public static double mToKm(double m) {
		return m / 1000.0;
	}
}
