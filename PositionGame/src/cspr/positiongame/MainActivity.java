package cspr.positiongame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.main);
	    }
	 
	 public void startGame(View v){
		 Intent i = new Intent(this,GameConfig.class);
		 startActivity(i);
	 }

}
