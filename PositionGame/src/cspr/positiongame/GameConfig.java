package cspr.positiongame;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class GameConfig extends ListActivity implements OnItemClickListener{
	
	private Integer[] imageDrawables = {
			R.drawable.ic_launcher ,
			R.drawable.ic_launcher ,
			R.drawable.ic_launcher ,
			R.drawable.ic_launcher ,
			R.drawable.ic_launcher
	};
	 
	private String[] imageComments = {
			"Mission1",
			"Mission2",
			"Mission3",
			"Mission4",
			"Mission5",
	};
	 
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.placelist_main);
			List<BindData> bindData = new ArrayList<BindData>();
			for(int i = 0; i< imageDrawables.length; i++) {
				BindData data = new BindData(imageComments[i], imageDrawables[i]);
				bindData.add(data);
			}
			setListAdapter(new ListViewAdapter(this, bindData));
			ListView listView = getListView();
			listView.setOnItemClickListener(this);
		}
	 
		class BindData {
			String imageComment;
			int imageDrawableId;
	 
			public BindData(String imageComment, int imageDrawableId) {
				this.imageComment = imageComment;
				this.imageDrawableId = imageDrawableId;
			}
		}
	 
		class ViewHolder {
			TextView textView;
			ImageView imageView;
		}
	 
		class ListViewAdapter extends ArrayAdapter {
			private LayoutInflater inflater;
	 
			public ListViewAdapter(Context context, List<BindData> objects) {
				super(context, 0, objects);
				this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
	 
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.placeitem, parent, false);
				holder = new ViewHolder();
				holder.textView = (TextView) convertView.findViewById(R.id.textView);
				holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
				convertView.setTag(holder);
			}
			else {
				holder = (ViewHolder) convertView.getTag();
			}
			BindData data = (BindData)getItem(position);
			holder.textView.setText(data.imageComment);
			holder.imageView.setImageResource(data.imageDrawableId);
			return convertView;
		}
	}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			Toast.makeText(this, "clicked!", Toast.LENGTH_LONG).show();
		}

}
